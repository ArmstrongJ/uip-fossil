       uIP/FOSSIL Version 1.2
       ======================

       Welcome to the wonderful world of TCP/IP on MS-DOS!  This package is
   a port of uIP Version 1.0 from Adam Dunkels to MS-DOS systems running
   FOSSIL (Fido-Opus-Seadog Standard Interface Layer) drivers.  The TCP/IP
   device is implemented as a SLIP (Serial Line Interface Protocol)
   connection via the FOSSIL driver.

       The implementation is compatible with SLIP drivers present on
   GNU/Linux systems and probably many others.  The driver implements
   normal SLIP (as opposed to CSLIP or even PPP) connections, which are a
   bit on the slow side.

       This port of uIP is licensed under the highly permissive modifed BSD
   license, as explained in the COPYING file.

       The source code encompasses a complete port of uIP to
   SLIP-over-FOSSIL setups.  As an example, the web server application has
   been modified and included in the source code.  Other uIP applications
   should be able to compile using the SLIP and FOSSIL code from this port
   with minimal to no modifications.

       Compiling
       =========

       uIP in general and the FOSSIL port in particular are designed to
   compile on almost any ANSI C compiler.  The code does not use any
   floating point mathematics.

       This uIP port was engineered to compile using either the OpenWatcom
   compiler or the Turbo C 2.0 compiler.  For OpenWatcom users, the uIP 
   project is contained in the topmost 'watcom' directory.  The entire 
   project is stored in 'uip-rb.wpj', and a single target, 'httpd.exe', 
   has been defined for this project.  Turbo C users can use the included
   makefile to build the project.

       Prior to building the uIP project, the FosLib15 library must be
   built.  The library provides a simplified C interface to the FOSSIL
   driver.  The library source code is available at:
   
       https://bitbucket.org/ArmstrongJ/foslib
       
   For both the OpenWatcom and Turbo C projects, FosLib15 should be
   built using a medium memory model.

       The Web Server
       ==============

       The example web server code included implements a rather simple, yet
   complete, http server solution.  To start the web server over a SLIP
   connection, use the following command:

       HTTPD <gateway ip> <local ip> <netmask> <com port> <speed>

       The com port is 0-based, meaning COM1 would be '0'.  The speed is
   any acceptable FOSSIL speed up to and including 38400 when applicable.

       The web server must be located in the same directory as the root web
   pages it will be serving.
   
       The web server will also check for equivalent, compressed files when
   the client reports it accepts content with gzip encoding.  Any HTML or 
   CSS files can be pre-compressed and stored on disk with the following
   extensions:
   
       .HTM -> .HGZ
       .CSS -> .CGZ
       .RSS -> .RGZ
       
   Again, the files will be served as an alternative if available.  The
   web server will never compress files itself; that task is left to the
   system administrator.

       Setting up SLIP on Linux
       ========================

       Information about configuring SLIP on Linux can be found at:

       http://jba.freeshell.org/uip-linux.html

       Questions and Support
       =====================

       Any questions about this port should be directed to PrintStar (Jeff
   Armstrong) at jeff@rainbow-100.com.

       ---
       Jeff Armstrong (aka PrintStar)
       jeff@rainbow-100.com
       http://jeff.rainbow-100.com/
       January 31, 2009
       Updated December 14, 2016

