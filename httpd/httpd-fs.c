/*
 * Copyright (c) 2009, 2016 Jeffrey Armstrong
 * All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack - MS-DOS FOSSIL port.
 * 
 * Author: Jeffrey Armstrong <jeff@rainbow-100.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "httpd-fs.h"

void safe_name(char *name)
{
#ifdef WATCOM
char *newtext;
    newtext = (char *)malloc((strlen(name)+1)*sizeof(char));
    memcpy(newtext,name,(strlen(name)+1)*sizeof(char));
    if(_islfn(name))
        _lfntosfn(newtext,name);
    free(newtext);
#endif
}

void change_to_backslash(char *name)
{
int counter;
    for(counter=0;counter<strlen(name);counter++) {
            if(name[counter]=='/') name[counter] = '\\';
    }
}

int compressed_name(char *str)
{
char *ext;

    if(str == NULL) 
        return -1;
    
    ext = strrchr(str, '.');
    if(ext == NULL || strlen(ext) < 4)
        return -1;
    
    ext[2] = 'g';
    ext[3] = 'z';
    return 1;
}

int compressed_available(const char *name)
{
char *cname;
char *noslash;
int ret;
struct stat st;

    if(name == NULL) return -1;    

    noslash = name;
    while(noslash[0] == '/') noslash++;
    if(noslash[0] == '\0') noslash = "index.htm";
    
    cname = (char *)malloc((strlen(noslash)+1)*sizeof(char));
    if(cname == NULL) return -1;

    strcpy(cname, noslash);
    ret = compressed_name(cname);

    if(ret == 1) {
        change_to_backslash(cname);
        safe_name(cname);

        if(stat(cname,&st) < 0)
            ret = -1;
    }
    
    free(cname);
    return ret;
}

int httpd_fs_open(char *name, struct httpd_fs_file *file)
{
FILE *fp;
int ret;
char *original;

    /* Any leading slashes must be stripped */
    original = NULL;
    if(name[0] == '/' || name[0] == '\\') {
        original = name;
        name++;
    }

    change_to_backslash(name);
    safe_name(name);
    fp = fopen(name,"rb");
    if(fp != NULL) {
    
        fseek(fp, 0, SEEK_END);
        file->len = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        file->fp = fp;
        
        ret = 1;
    } else
        ret = -1;

    if(original != NULL)
        name = original;

    return ret;
}

void httpd_fs_init(void)
{
    return;
}
