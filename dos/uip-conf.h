/*
 * Copyright (c) 2009 Jeffrey Armstrong
 * All rights reserved. 
 *
* Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack - MS-DOS FOSSIL port.
 * 
 * Author: Jeffrey Armstrong <jeff@rainbow-100.com>
 *
 */

#ifndef UIP_CONF_HEADERS__
#define UIP_CONF_HEADERS__

#define UIP_CONF_STATISTICS     0
#undef UIP_COMPILE_STATISTICS
#define UIP_CONF_UDP 0
#define UIP_STATS UIP_CONF_STATISTICS

#define UIP_CONF_BYTE_ORDER UIP_LITTLE_ENDIAN

#define UIP_CONF_MAX_CONNECTIONS    50

#define UIP_CONF_BUFFER_SIZE        1500

typedef unsigned char u8_t;
typedef unsigned int u16_t;

/* Some application-specific settings... */
#include "app.h"
#include "main.h"

typedef long uip_stats_t;
typedef void* uip_udp_appstate_t;
/* typedef void* uip_tcp_appstate_t; */

#endif /* UIP_CONF_HEADERS__ */
