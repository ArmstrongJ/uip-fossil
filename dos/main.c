/*
 * Copyright (c) 2009 Jeffrey Armstrong
 * All rights reserved. 
 *
* Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack - MS-DOS FOSSIL port.
 * 
 * Author: Jeffrey Armstrong <jeff@rainbow-100.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "httpd.h"

#include "main.h" /* Must come first - contains necessary declaration of
                   * uip_tcp_appstate
                   */

#include "fossil.h"
#include "uip.h"
#include "uip_arp.h"
#include "slipdev.h"
#include "slipfos.h"


#include "timer.h"

#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

int ip_from_string(const char *string, int ip[4])
{
char *token;
char *original;
int i, ret;

    for(i=0;i<4;i++) ip[i] = 0;
    if(string == NULL) return -1;

    original = (char *)malloc((strlen(string)+1)*sizeof(char));
    memcpy(original,string,strlen(string)+1);
    
    ret = 1;
    token = strtok(original,".");
    for(i=0;i<4;i++) {
        if(token == NULL) {
            ret = -1;
            break;
        }
        ip[i] = atoi(token);
        token = strtok(NULL,".");
    }

    token = NULL;
    free(original);
    
    return ret;
}

void header() {
    printf("+----------------------------------------------------------+\n");
    printf("|            uIP/FOSSIL HTTP Server Version 1.2            |\n");
    printf("|          Copyright 2009, 2016 Jeffrey Armstrong          |\n");
    printf("| Portions Copyright Swedish Institute of Computer Science |\n");
    printf("|             Portions Copyright Adam Dunkels              |\n");
    printf("+----------------------------------------------------------+\n\n");
}

int main(int argc, char *argv[])
{
int i;
uip_ipaddr_t ipaddr;
struct timer periodic_timer;

int ip[4];

    header();
    
    if(argc != 6) {
        printf("Usage: %s <host ip> <local ip> <netmask> <comport> <baud>\n",argv[0]);
        return 0;
    }

    timer_set(&periodic_timer, CLOCK_SECOND / 2);

    /* Initialize the FOSSIL driver here */
    printf("Initializing FOSSIL driver...");
    if(slip_fossil_init(atoi(argv[4]),atoi(argv[5])) < 0) {
        printf("FAILED\nExiting...\n");
        return 0;
    }
    printf("OK\n");

    printf("Initializing SLIP interface...");
    slipdev_init();
    printf("OK\n");

    printf("Initializing uIP stack...");
    uip_init();
    printf("OK\n");
    
    /* Set IP and netmask from command line arguments */
    for(i=1;i<4;i++) {
        if(ip_from_string(argv[i],ip) < 0) {
            printf("Error parsing ip/netmask in argument %d: %s\nMake sure this argument is a valid 4-element dot-delimited string.\n",i,argv[i]);
            return 0;
        } else {
            uip_ipaddr(&ipaddr, ip[0], ip[1], ip[2], ip[3]);
            switch(i) {
                case 1:
                    uip_setdraddr(&ipaddr);
                    break;
                case 2:
                    uip_sethostaddr(&ipaddr);
                    break;
                case 3:
                    uip_setnetmask(&ipaddr);
                    break;
            }
        }
    }

    printf("Initializing httpd server...");
    httpd_init();
    printf("OK\n");

#ifdef INITIALIZE_ONLY
    return 1;
#endif

    uip_gethostaddr(&ipaddr);
    printf("Server is now running - press any key to exit...\n");
    while(1) {
        uip_len = slipdev_poll();
        if(uip_len > 0) {

#ifdef DEBUG
            printf("data received: %d (%x)\n",uip_len,(int)BUF->type);
#endif

            uip_input();

            /* If the above function invocation resulted in data that
                should be sent out on the network, the global variable
                uip_len is set to a value > 0. */

            if(uip_len > 0) {

#ifdef DEBUG
                printf("output ready: %d\n",uip_len);
#endif

                slipdev_send();
            }
        } else if(timer_expired(&periodic_timer)) {
            timer_reset(&periodic_timer);
            for(i = 0; i < UIP_CONNS; i++) {
                uip_periodic(i);
                /* If the above function invocation resulted in data that
                    should be sent out on the network, the global variable
                    uip_len is set to a value > 0. */
                if(uip_len > 0) {
                    slipdev_send();
                }
            }
        }

        if(Kb_Hit() == 1)
            break;
    }


    return 0;
}

void uip_log(char *m)
{
#ifdef DEBUG
    printf("uIP log message: %s\n", m);
#endif
}
