/* FOSSIL SLIP interface for uip
   Implements slipdev_char_poll and slipdev_char_put */
   
/*
 * Copyright (c) 2005, 2009 Jeffrey Armstrong
 * All rights reserved. 
 *
* Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack - MS-DOS FOSSIL port.
 * 
 * Author: Jeffrey Armstrong <jeff@rainbow-100.com>
 *
 */

#include <stdio.h>
#include "uip.h"
#include "fossil.h"
#include "slipfos.h"

#define DEFAULT_BAUD    9600

int slipdev_char_poll_2()
{
unsigned char in;
int return_val;

        if(F_Peek_Check()) {

          in =  Get_Raw();

#ifdef DEBUG_FOSSIL
            printf("Actual poll value = %d\n",in);
#endif

          return_val = (int)in;

        } else {
                return_val = -1;
        }
        return return_val;

}

u8_t slipdev_char_poll(u8_t *c)
{
unsigned char in;
u8_t return_val;

        /* if(F_Peek() && F_Get_Status(CK_DCD)) { */
            
        if(F_Peek_Check()) {

          in =  Get_Raw();
                        
#ifdef DEBUG_FOSSIL
            printf("Actual poll value = %d\n",in);
#endif
                                
          *c = (u8_t)in;
          return_val = (u8_t)1;
        
        } else {
                return_val = (u8_t)0;
        }
        return return_val;
}

void slipdev_char_put(u8_t c)
{
        F_Send_Char(TRANSMIT, (unsigned char)c);
}

int slip_fossil_init(int desired_port, int speed)
{
        port = desired_port;

        printf("SLIP FOSSIL Settings - Port: %d Speed: %d\n",port,speed);

        if(!F_Init()) {
                printf("FOSSIL initialization failed\n");
                return -1;
        }
        if (!F_Set_Baud(speed)) {
                printf("FOSSIL baud set failed\n");
                return -2;
        }
        /*Fl_Ctrl(TXONXOFF); */

        /* slattach uses hardware flow control */
        Fl_Ctrl(CTSRTS);

        F_Dtr(TRUE);
        printf("DTR has been raised!\n");
        
        return 1;
}
