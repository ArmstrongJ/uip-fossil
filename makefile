# Makefile for use with Turbo C 2.0/2.01
#
# Please note that Turbo C may fail to compile some files...

CC=tcc
LINK=tlink

CFLAGS=-O -G -Z -Iuip -Ihttpd -Ilib -Islip -Idos -I..\foslib -mm
LFLAGS=-O -G -Z -mm

LIBCMD=tlib
APPLIB=httpd.lib
FOSSILLIB=..\foslib\fossil.lib

clean:
	del *.obj
	del *.exe
	del *.lib

uipcore: uip\psock.c uip\timer.c uip\uip_arp.c uip\uip.c uip\uip-fw.c uip\uiplib.c uip\uip-nb.c uip\uip-splt.c lib\memb.c dos\clk_arch.c
psock.obj: uip\psock.c
	$(CC) $(CFLAGS) -c uip\psock.c
timer.obj: uip\timer.c
	$(CC) $(CFLAGS) -c uip\timer.c
uip_arp.obj: uip\uip_arp.c
	$(CC) $(CFLAGS) -c uip\uip_arp.c
uip.obj: uip\uip.c
	$(CC) $(CFLAGS) -c uip\uip.c
uiplib.obj: uip\uiplib.c
	$(CC) $(CFLAGS) -c uip\uiplib.c
uip_splt.obj: uip\uip-splt.c
	$(CC) $(CFLAGS) -c -ouip_splt.obj uip\uip-splt.c
uip_fw.obj: uip\uip-fw.c
	$(CC) $(CFLAGS) -c -ouip_fw.obj uip\uip-fw.c
uip_nb.obj: uip\uip-nb.c
	$(CC) $(CFLAGS) -c -ouip_nb.obj uip\uip-nb.c
uip.lib: psock.obj timer.obj uip_arp.obj uip.obj uiplib.obj uip_splt.obj uip_fw.obj uip_nb.obj
	$(LIBCMD) uip.lib -+psock.obj -+timer.obj -+uip_arp.obj
	$(LIBCMD) uip.lib -+uip.obj -+uip_fw.obj -+uiplib.obj
	$(LIBCMD) uip.lib -+uip_nb.obj -+uip_splt.obj 
	
http_str.obj: httpd\http-str.c
	$(CC) $(CFLAGS) -c -ohttp_str.obj httpd\http-str.c
httpd_fs.obj: httpd\httpd-fs.c
	$(CC) $(CFLAGS) -c -ohttpd_fs.obj httpd\httpd-fs.c
httpd.obj: httpd\httpd.c
	$(CC) $(CFLAGS) -c httpd\httpd.c
httpd.lib: http_str.obj httpd_fs.obj httpd.obj
	$(LIBCMD) httpd.lib -+http_str.obj -+httpd_fs.obj -+httpd.obj

slipdev.obj: slip\slipdev.c
	$(CC) $(CFLAGS) -c slip\slipdev.c
slipfos.obj: slip\slipfos.c
	$(CC) $(CFLAGS) -c slip\slipfos.c
slip.lib: slipdev.obj slipfos.obj
	$(LIBCMD) slip.lib -+slipfos.obj -+slipdev.obj

memb.obj: lib\memb.c
	$(CC) $(CFLAGS) -c lib\memb.c
clk_arch.obj: dos\clk_arch.c
	$(CC) $(CFLAGS) -c dos\clk_arch.c
util.lib: memb.obj clk_arch.obj
        $(LIBCMD) util.lib -+memb.obj -+clk_arch.obj

main.o: dos\main.c
	$(CC) $(CFLAGS) -omain.o -c dos\main.c

httpd.exe: uip.lib httpd.lib slip.lib util.lib dos\main.c
        copy $(FOSSILLIB) f.lib
	$(CC) $(CFLAGS) -ehttpd.exe dos\main.c util.lib slip.lib httpd.lib uip.lib f.lib
        del f.lib

sertest.exe: sertest.c
        $(CC) $(CFLAGS) -esertest.exe sertest.c $(FOSSILLIB)

all: httpd.exe
